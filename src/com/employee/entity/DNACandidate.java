package com.employee.entity;

import java.time.LocalDateTime;

public class DNACandidate {
	private int employeeId;
	private String employeeName;
	private LocalDateTime joiningDate;
	private int experience;

	public DNACandidate(int i, String string, LocalDateTime of, int j) {
		// TODO Auto-generated constructor stub
		this.employeeId = i;
		this.employeeName = string;
		this.joiningDate = of;
		this.experience = j;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public LocalDateTime getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(LocalDateTime joiningDate) {
		this.joiningDate = joiningDate;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	@Override
	public String toString() {
		return "DNACandidate [employeeId=" + employeeId + ", employeeName=" + employeeName + ", joiningDate=" + joiningDate
			 + ", experience=" + experience + "]";
	}
}
