package com.employee.entity;

import java.time.LocalDateTime;

public class Employee {
	private int employeeId;
	private String employeeName;
	private LocalDateTime joiningDate;
	private Boolean isManager;
	private int experience;
	public Employee(int i, String string, LocalDateTime of, boolean b, int j) {
		// TODO Auto-generated constructor stub
		this.employeeId=i;
		this.employeeName=string;
		this.joiningDate=of;
		this.isManager=b;
		this.experience=j;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public LocalDateTime getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(LocalDateTime joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Boolean getIsManager() {
		return isManager;
	}
	public void setIsManager(Boolean isManager) {
		this.isManager = isManager;
	}
	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", joiningDate=" + joiningDate
				+ ", isManager=" + isManager + ", experience=" + experience + "]";
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
}
