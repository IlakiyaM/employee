package com.employee.main;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.*;

import com.employee.entity.*;

public class EmployeeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(new Employee(10011, "Reena", LocalDateTime.of(2019, Month.MARCH, 17, 14, 33, 48), false, 4));
		employeeList.add(new Employee(10013, "Veena", LocalDateTime.of(2020, Month.JANUARY, 27, 14, 33, 48), false, 6));
		employeeList.add(new Employee(10012, "Mary", LocalDateTime.of(2019, Month.APRIL, 28, 14, 33, 48), true, 10));
		employeeList.add(new Employee(10015, "Meena", LocalDateTime.of(2018, Month.JULY, 28, 14, 33, 48), false, 6));
		employeeList.add(new Employee(10014, "Leena", LocalDateTime.of(2020, Month.MARCH, 20, 14, 33, 48), true, 9));
		employeeList
				.add(new Employee(10016, "Rohan", LocalDateTime.of(2019, Month.DECEMBER, 22, 14, 33, 48), false, 5));
		employeeList.add(new Employee(10017, "Hrithik", LocalDateTime.of(2020, Month.AUGUST, 18, 14, 33, 48), true, 8));
		employeeList.forEach(System.out::println);

		// Printing 1 - 100
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("---Printing 1 to 100---");
		IntStream closedRangeStream = IntStream.rangeClosed(1, 100);
		closedRangeStream.forEach(System.out::println);

		// Top 5 employees based on joining date
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("---Printing top 5 employees based on Joining Date---");
		List<Employee> emp = employeeList.stream()
				.sorted((e1, e2) -> e1.getJoiningDate().compareTo(e2.getJoiningDate())).limit(5)
				.collect(Collectors.toList());
		emp.forEach(System.out::println);

		
		// Get employees based on JoiningDate input (it should be with time)
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("---Printing employees based on given Joining Date with time---");
		LocalDateTime date = LocalDateTime.of( 2018, Month.JULY, 28, 14, 33, 48 ); 
		employeeList.stream().filter( e -> e.getJoiningDate().isEqual(date) )
			    .collect( Collectors.toList() ).forEach(System.out::println);
		
		// Getting all working days for next week
		LocalDate today = LocalDate.of(2021, 1, 25);
		System.out.println("\n");
		System.out.println("---Printing working days of next week---");
		System.out.println(countBusinessDaysBetween(today, today.plusDays(7)) + " Working days");


	
	//Categorize employees as regular and Manager based on their isManager field
		System.out.println("\n");
		System.out.println("---Printing Regular Employees---");
		employeeList.stream().filter(e-> e.getIsManager().equals(false)).collect(Collectors.toList()).forEach(System.out::println);
		System.out.println("\n");
		System.out.println("---Printing Employees who are Managers---");
		employeeList.stream().filter(e-> e.getIsManager().equals(true)).collect(Collectors.toList()).forEach(System.out::println);
	}

	private static long countBusinessDaysBetween(LocalDate startDate, LocalDate endDate) {
		if (startDate == null || endDate == null) {
			throw new IllegalArgumentException(
					"Invalid method argument(s) to countBusinessDaysBetween(" + startDate + "," + endDate + ")");
		}

		Predicate<LocalDate> isWeekend = date -> date.getDayOfWeek() == DayOfWeek.SATURDAY
				|| date.getDayOfWeek() == DayOfWeek.SUNDAY;

		long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);

		long businessDays = Stream.iterate(startDate, date -> date.plusDays(1)).limit(daysBetween)
				.filter((isWeekend).negate()).count();
		return businessDays;
	}
}
